#!/usr/bin/python
# Import required libraries
import sys
import time
import RPi.GPIO as GPIO
from gpiozero import MCP3008

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Define GPIO signals to use
# Physical pins 11,15,16,18
# GPIO17,GPIO22,GPIO23,GPIO24
motorPins = [[17, 22, 23, 24],
			[5, 6, 7, 25],
			[12, 13, 16, 26]]

sensorPins = [0, 1, 2]
sensorThreshold = 0.95
releaseCount = 250

# Define advanced sequence
# as shown in manufacturers datasheet
Seq = [[1, 0, 0, 1],
	   [1, 0, 0, 0],
	   [1, 1, 0, 0],
	   [0, 1, 0, 0],
	   [0, 1, 1, 0],
	   [0, 0, 1, 0],
	   [0, 0, 1, 1],
	   [0, 0, 0, 1]]

StepCount = len(Seq)
motorStepDirections = [-2,-2,-2]


# Read wait time from command line

WaitTime = 10/float(1000)

#set GPIO Pins
sonarTriggerPin = 21
sonarEchoPin = 20

def distance():
    # set Trigger to HIGH
    GPIO.output(sonarTriggerPin, True)

    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(sonarTriggerPin, False)

    StartTime = time.time()
    StopTime = time.time()

    # save StartTime
    while GPIO.input(sonarEchoPin) == 0:
        StartTime = time.time()

    # save time of arrival
    while GPIO.input(sonarEchoPin) == 1:
        StopTime = time.time()

    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (TimeElapsed * 34300) / 2

    return distance


def stepMotor(motorIndex, motorStepCounters):
	
	StepPins = motorPins[motorIndex]
	StepDir = motorStepDirections[motorIndex]
	StepCounter = motorStepCounters[motorIndex]

	for pin in range(0, 4):
		xpin = StepPins[pin]
		if Seq[StepCounter][pin] != 0:
			# print " Enable GPIO %i" % (xpin)
			GPIO.output(xpin, True)
		else:
			GPIO.output(xpin, False)

	StepCounter += StepDir

  # If we reach the end of the sequence
  # start again
	if (StepCounter >= StepCount):
		StepCounter = 0
	if (StepCounter < 0):
		StepCounter = StepCount+StepDir

	motorStepCounters[motorIndex] = StepCounter
	

def release():
	global motorStepDirections
	global stepsToClose

	motorStepDirections = [x*-1 for x in motorStepDirections]
	motorStepCounters = [0] * len(motorPins)
	counter = 0
	motors = len(motorPins)

	counters = [min(releaseCount, steps) for steps in stepsToClose]
	while True:
		
		for i in range(motors):
			if counters[i]<=0:
				continue
			counters[i]-=1
			stepMotor(i, motorStepCounters)
			
		if max(counters) <=0:
			break
		# Wait before moving on
		time.sleep(WaitTime)
	
	motorStepDirections = [x*-1 for x in motorStepDirections]
	stepsToClose=[0]*motors

def beginGrab():
	motorStepCounters = [0] * len(motorPins)
	activeMotors = [True] * len(motorPins)
	print("Begin grabbing")
	while True:

		for sensorIndex in range(len(sensors)):
			activeMotors[sensorIndex] = sensors[sensorIndex].value > sensorThreshold and activeMotors[sensorIndex]
		
		if True not in activeMotors:
			print("Object grabbed")
			try:
				input("Press enter to release")
			except Exception:
				pass
			release()
			return

		for i in range(len(activeMotors)):
			if activeMotors[i]:
				stepsToClose[i]+=1
				stepMotor(i, motorStepCounters)
		# Wait before moving on
		time.sleep(WaitTime)
		

# Set all pins as output
for motorPin in motorPins:
	for pin in motorPin:
		print "Setup pins"
		GPIO.setup(pin, GPIO.OUT)
		GPIO.output(pin, False)

#set GPIO direction (IN / OUT)
GPIO.setup(sonarTriggerPin, GPIO.OUT)
GPIO.setup(sonarEchoPin, GPIO.IN)

sensors = []
for sensorPin in sensorPins:
	sensors.append(MCP3008(sensorPin))

stepsToClose = [releaseCount]*len(motorPins)
release()
# Start main loop
while True:
	dist = distance()
	print("Object is %.1fcm away"%dist)
	if dist<10:
		beginGrab()
